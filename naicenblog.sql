-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2019-11-11 03:11:19
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `naicenblog`
--

-- --------------------------------------------------------

--
-- 表的结构 `naicen_admin`
--

CREATE TABLE IF NOT EXISTS `naicen_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `userpwd` char(32) NOT NULL,
  `loadtime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `naicen_admin`
--

INSERT INTO `naicen_admin` (`id`, `username`, `userpwd`, `loadtime`) VALUES
(1, 'naicenblog', '7d3dbbf68171675b7682dab6d46b1697', '2017/01/01');

-- --------------------------------------------------------

--
-- 表的结构 `naicen_conf`
--

CREATE TABLE IF NOT EXISTS `naicen_conf` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `keywords` varchar(255) NOT NULL,
  `descc` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `style` varchar(255) NOT NULL,
  `backimg` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `naicen_conf`
--

INSERT INTO `naicen_conf` (`id`, `keywords`, `descc`, `logo`, `status`, `style`, `backimg`, `title`) VALUES
(1, 'NaicenBlog,Blog,网络日志', 'NaicenBlog网络日志系统', '--', '1', 'green', '--', 'NaicenBlog｜NaicenBlog个人网络日志');

-- --------------------------------------------------------

--
-- 表的结构 `naicen_fun`
--

CREATE TABLE IF NOT EXISTS `naicen_fun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `ico` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `naicen_fun`
--

INSERT INTO `naicen_fun` (`id`, `name`, `url`, `ico`) VALUES
(1, '日记', 'list.php', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `naicen_list`
--

CREATE TABLE IF NOT EXISTS `naicen_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `naicen_notepad`
--

CREATE TABLE IF NOT EXISTS `naicen_notepad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `times` varchar(50) NOT NULL,
  `lid` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- 转存表中的数据 `naicen_notepad`
--

INSERT INTO `naicen_notepad` (`id`, `title`, `content`, `uname`, `status`, `times`, `lid`) VALUES
(1, 'HELLO WORD!', '开始您的新旅途吧！', 'NaicenBlog', '1', '2019年11月10日', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
