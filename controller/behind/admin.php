<?php
header("Content-type:text/html;charset=utf-8");
/*------------------------------------------------------------------------------------------------------
 *版权所有：NaicenBlog 1.0
 *文件名: admin.php
 *文件路径：/controller/behind/admin.php
 *文件类型：PHP文件
 *文件功能描述: 后台登陆控制器
 *作者: NaicenLee
 *时间: 2019/11/10 11:02:05
 *创建时间: 2017/1/30/ 21:43
 *修改时间：2019/11/10 11:02:23
 *修改描述：1.0版本重构
 *邮箱：3097347965@qq.com
 *备注：
 -----------------------------------------------------------------------------------------------------*/


$POST=$_POST;

$user=$POST['user'];
$pwd=md5($POST['pwd']);

include("../../config/dirconf.php");
include_once(WORKDIR.M.'dbconn.php');

$sql='select * from '.TB_PREFIX.'admin where username="'.$user.'";';
$res = $pdo->query($sql);
foreach($res->fetchAll()as $R){}

if(!$res){
	
  echo'<script>
  		    alert("该用户名不存在");
  			location.href="'.WORKCVDIR2.'/controller/behind/index.php";
  		</script>';
}elseif($pwd!=$R[2]){

	echo'<script>
			alert("密码错误！");
  			location.href="'.WORKCVDIR2.'/controller/behind/index.php";
  		</script>';
}else{

session_start();
$_SESSION['load']='1';

echo'<script>
  		location.href="'.WORKCVDIR2.'/view/behind/index.php";
  	</script>';
}
$pdo=null;