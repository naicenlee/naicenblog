<?php 
header("Content-type:text/html;charset=utf-8");
/*------------------------------------------------------------------------------------------------------
 *版权所有：NaicenBlog 1.0
 *文件名: del.php
 *文件路径：/controller/behind/del.php
 *文件类型：PHP文件
 *文件功能描述:通用删除数据控制器
 *作者: NaicenLee
 *时间: 2019/11/10 11:05:38
 *创建时间: 2017/1/30/ 21:50
 *修改时间：2019/11/10 20:04:39
 *修改描述：1.0版本重构
 *邮箱：3097347965@qq.com
 *备注：
 -----------------------------------------------------------------------------------------------------*/

include("loadtest.php");
$GET=$_GET;
$tname=$GET['table'];
$id=$GET['id'];
$action=$GET['action'];

switch($tname){
	case'fun':	
		if($action=='del'){
				include("../../config/dirconf.php");  
				include_once(WORKDIR.M.'dbconn.php');   
			   $sql='delete from '.TB_PREFIX.$tname.' where id='.$id.';';
			   $res=$pdo->query($sql);
			   if($res){
                $pdo=null;
			   	echo'
			   			<script>
			   				alert("删除成功！");
			   				//history.back();//返回没有刷新，使用location跳转
			   				location.href="'.WORKCVDIR2.V_B.'admin/funshow.php";
			   			</script>
			   			';
				}
            }
	break;
    
	case'notepad':
		if($action=='del'){
			include("../../config/dirconf.php");  
			include_once(WORKDIR.M.'dbconn.php');   
			$sql='delete from '.TB_PREFIX.$tname.' where id='.$id.';';
			$res=$pdo->query($sql);
			if($res){
                $pdo=null;
				echo'
			   			<script>
			   				alert("删除成功！");
			   				location.href="'.WORKCVDIR2.V_B.'admin/notepadshow.php";
			   			</script>
			   			';
			}
		
		}
        
		break;
}