<?php
header("Content-type:text/html;charset=utf-8");
/*------------------------------------------------------------------------------------------------------
 *版权所有： NaicenBlog 1.0
 *文件名: index.php
 *文件路径：/index.php
 *文件类型：PHP文件
 *文件功能描述: 入口文件
 *作者: NaicenLee
 *时间: 2019/11/10 9:40:35
 *创建时间: 2017/1/25 22:13
 *修改时间：2019/11/10 9:41:47
 *修改描述：1.0版本代码重构
 *邮箱：3097347965@qq.com
 *备注：
 -----------------------------------------------------------------------------------------------------*/

include('controller/front/index.php');
?>