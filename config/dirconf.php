﻿<?php
header("Content-type:text/html;charset=utf-8");
/*------------------------------------------------------------------------------------------------------
 *版权所有：NaicenBlog 1.0
 *文件名: dirconf.php
 *文件路径：/config/dirconf.php
 *文件类型：PHP文件
 *文件功能描述: 通用目录配置文件
 *作者: NaicenLee
 *时间: 2019/11/10 11:16:56
 *创建时间: 2017/1/30/ 20:17
 *修改时间：2019/11/10 11:17:18
 *修改描述：1.0版本重构
 *邮箱：3097347965@qq.com
 *备注：
 -----------------------------------------------------------------------------------------------------*/

define('ROOT_DIR',$_SERVER['DOCUMENT_ROOT']);//定义绝对的根应用目录

//mvc文件目录 
define('V','/view/');//定义视口目录
define('V_F','/view/front/'); //定义前段视口目录
define('V_B','/view/behind/');//定义后端视口目录

define('M','/model/'); //定义模型目录
define('M_F','/model/front/');//定义前端模型
define('M_B','/model/behind/');//定义后端模型目录

define('C','/controller/');//定义控制器目录
define('C_F','/controller/front/');//定义前端控制器目录
define('C_B','/controller/behind/');//定义后端控制器目录

define('C_N','/config/'); //定义配置文件目录
define('C_NF','/config/front/');//定义配置前端目录
define('C_NB','/config/behind/');//定义配置后端目录

//工作目录
define('WORKDIR',ROOT_DIR.'/htdocs');
define('WORKCVDIR2','/htdocs');

