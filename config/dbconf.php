<?php
header("Content-type:text/html;charset=utf-8");
/*------------------------------------------------------------------------------------------------------
 *版权所有：NaicenBlog 1.0
 *文件名: dbconf.php
 *文件路径：/config/dbconf.php
 *文件类型：PHP文件
 *文件功能描述: 数据库配置文件
 *作者: NaicenLee
 *时间: 2019/11/10 11:18:14
 *创建时间: 2017/1/26/ 8:31
 *修改时间：2019/11/10 11:18:49
 *修改描述：1.0版本重构
 *邮箱：3097347965@qq.com
 *备注：
 -----------------------------------------------------------------------------------------------------*/

define('DB_HOST','localhost');
define('DB_USER','naicen');
define('DB_PWD','123456');
define('DB_NAME','naicenblog');
define('DB_PORT','3306');
define('DB_CHARSET','utf8');
define('TB_PREFIX','naicen_');
define('TIMEZONE','prc');