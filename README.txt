我的博客1.0版本

使用原生php语言编写 php+mysql+apache  
1.0版本其主要几个简单的功能

前台
1.前台主页图标链接类似图标界面风格
2.日记列表页
3.日记内容页

后台
1.对网站的名称，关键字，描述，主页风格等配置
2.独立的主页图标链接添加和管理
3.日记的编写与管理
4.退出系统
5.后台登陆身份验证

上线使用的时候需要修改的文件
1，目录配置文件 dirconf.php
2.数据库配置文件 dbconf.php

本系统所有权利归属NaienLee所有
如有疑问请联系作者邮箱3097347965@qq.com

【日志】
【2019/11/9】
	1.对原1.0更名为NaicenBlog1.0，对原代码进行修改。
	2.dbconn文件mysql()函数已被弃用，使用PDO重构。
【2019/11/10】
	1.对1.0版本重构，规范代码风格并去除代码注释，让代码更简洁。	
	2.增加闭站维护管理员登入功能。
    3.去除后台自行添加功能板块，便于后面2.0版本以插件化管理。
    4.修改主页风格。
    5.更换日记主页图标。
    6.增加主页三种颜色风格，暗黑、清新、天空。
    7.修改日记列表页样式。
    8.修改日记详情页样式。
    9.修改登录页样式。
    10.管理员登入新页面打开。
    11.增加后台管理员修改密码功能。
    
 【安装步骤】
    1.克隆项目到您的计算机。
    2.解压缩文件，将naicenblog目录内的文件全部上传至您的网站空间FTP。
    3.打开您的主机数据库PhpAdmin,将项目中的naicenblog.sql导入您的数据库。
    4.配置工作目录，将ftp上的config目录下的dirconf.php文件下载下来，修改
              define('WORKDIR',ROOT_DIR.'/htdocs');
              define('WORKCVDIR2','/htdocs');
        为
               define('WORKDIR',ROOT_DIR.'');
               define('WORKCVDIR2','');
       上传覆盖。
    5.配置数据库信息，下载config目录下的dbconf.php  修改
                define('DB_HOST','localhost');
                define('DB_USER','naicen');
                define('DB_PWD','123456');
                define('DB_NAME','naicenblog');
        为您的数据库信息
        例如：
                define('DB_HOST','默认为localhost');
                define('DB_USER','数据库用户名');
                define('DB_PWD','数据库尼玛');
                define('DB_NAME','数据库名');
        上传覆盖。
    
    做完以上步骤，您可以用浏览器打开您的网址查看是否工作正常。
    默认后台账户和密码为：naicenblog 
    演示地址：http://naicenlee.byethost15.com
    github主页:naicenlee.github.io
    由于平时比较忙,这个只是个人爱好.
    软件作者首页:http://认识自己的无知是认识世界的最可靠的方法.top
    IRC讨论频道:freenode #PPO
    