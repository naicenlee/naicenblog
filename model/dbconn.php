﻿<?php
header("Content-type:text/html;charset=utf-8");
/*------------------------------------------------------------------------------------------------------
 *版权所有：NaicenBlog 1.0
 *文件名: dbconn.php
 *文件路径：/model/dbonn.php 
 *文件类型：PHP文件
 *文件功能描述: 数据库连接模型
 *作者: NaicenLee
 *时间: 2019/11/10 10:56:43
 *创建时间: 2017/1/27/ 23:19
 *修改时间：2019/11/11 9:24:58
 *修改描述：1.0版本重构
 *邮箱：3097347965@qq.com
 *备注：
 -----------------------------------------------------------------------------------------------------*/

include_once(WORKDIR.C_N.'dbconf.php');
$DBchar=array(PDO::MYSQL_ATTR_INIT_COMMAND => 'set names '.DB_CHARSET);
$pdo=new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';port='.DB_PORT,DB_USER,DB_PWD,$DBchar);



